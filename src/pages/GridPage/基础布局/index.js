import { Col, Row } from 'antd';
import styles from "./index.module.less"
/**
 * 1.使用 offset 可以将列向右侧偏。例如，offset={4} 将元素向右侧偏移了 4 个列（column）的宽度。
 * 2.justify 子元素根据不同的值 start、center、end、space-between、space-around 和 space-evenly，分别定义其在父节点里面的排版方式。
 * 3.align 设置垂直属性 bottom  middle  top
 * 4.通过 order 来改变元素的排序。
 */

const DemoBox = (props) => <p style={{ height: props.value }}>{props.children}</p>;

const GridBasePage = () =>{
    return(
        <div>
            <Row>
                <Col className={styles.colStyle} span={24}>col</Col>
            </Row>
            <Row>
                <Col className={styles.colStyle} span={7}>col</Col>
                <Col className={styles.colStyle} span={7}>col</Col>
                <Col className={styles.colStyle} span={7}>col</Col>
            </Row>
            <Row>
                <Col className={styles.colStyle} span={5}>col</Col>
                <Col className={styles.colStyle} span={2} offset={4}>col</Col>
            </Row>

            <Row justify="center">
                <Col className={styles.colStyle} span={5}>col</Col>
                <Col className={styles.colStyleOne} span={5}>col</Col>
                <Col className={styles.colStyle} span={5}>col</Col>
                <Col className={styles.colStyleOne} span={5}>col</Col>
            </Row>
            <Row justify="space-between">
                <Col className={styles.colStyle} span={5}>col</Col>
                <Col className={styles.colStyleOne} span={5}>col</Col>
                <Col className={styles.colStyle} span={5}>col</Col>
                <Col className={styles.colStyleOne} span={5}>col</Col>
            </Row>
            <Row justify="space-around">
                <Col className={styles.colStyle} span={5}>col</Col>
                <Col className={styles.colStyleOne} span={5}>col</Col>
                <Col className={styles.colStyle} span={5}>col</Col>
                <Col className={styles.colStyleOne} span={5}>col</Col>
            </Row>
            <Row justify="center" align="top">
                <Col className={styles.colStyle} span={5}>
                    <DemoBox value={100}>col-4</DemoBox>
                </Col>
                <Col className={styles.colStyleOne} span={5}>
                    <DemoBox value={50}>col-4</DemoBox>
                </Col>
                <Col className={styles.colStyle} span={5}>
                    <DemoBox value={120}>col-4</DemoBox>
                </Col>
                <Col className={styles.colStyleOne} span={5}>
                    <DemoBox value={80}>col-4</DemoBox>
                </Col>
            </Row>
            <Row justify="center" align="bottom">
                <Col className={styles.colStyle} span={5}>
                    <DemoBox value={100}>col-4</DemoBox>
                </Col>
                <Col className={styles.colStyleOne} span={5}>
                    <DemoBox value={50}>col-4</DemoBox>
                </Col>
                <Col className={styles.colStyle} span={5}>
                    <DemoBox value={120}>col-4</DemoBox>
                </Col>
                <Col className={styles.colStyleOne} span={5}>
                    <DemoBox value={80}>col-4</DemoBox>
                </Col>
            </Row>
        </div>
    )
}
export default GridBasePage
export {DemoBox}