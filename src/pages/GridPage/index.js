import {Button, Flex} from "antd"
import { Outlet } from "react-router-dom"

const GridPage =()=>{
    return(
        <Outlet/>
    )
}

export default GridPage