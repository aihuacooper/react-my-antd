import { Button, Flex, Card, Space, Avatar,List, Drawer, Spin,notification, Empty, Divider, Tour, Skeleton, Radio, message, Alert, Modal, Popconfirm, Watermark, Result, Progress } from 'antd';
import { PlusOutlined,MinusOutlined,AntDesignOutlined,EllipsisOutlined,ExclamationCircleFilled } from '@ant-design/icons';
import { useEffect, useRef, useState,createContext } from 'react';
import Marquee from 'react-fast-marquee';
import { green, red } from '@ant-design/colors';

/**
 * 1.Alert 警告提示
 * 2.Marquee 跑马灯
 * 3.Drawer 抽屉
 * 4.Message 全局提示
 * 5.Modal 对话框
 */

/**
 * Alert 警告提示 AND  Marquee 跑马灯
 */
const AlertWidget =()=>{
    const onClose=()=>{
        message.error("删除了~~~")
    }
    return(
        <Card title="Alert 警告提示">
            <Space size={"large"}>
                <Space direction='vertical' style={{width:300}} >
                    <Alert message="Success Text" type={"success"} showIcon/>
                    <Alert message="Info Text" type="info" closable showIcon/>
                    <Alert message="Warning Text" type="warning" closable onClose={onClose} showIcon/>
                    <Alert 
                    message="我是标题" 
                    description={"我是介绍内容水电费水电费水电费"} 
                    type="error" 
                    closable 
                    onClose={onClose}
                    />
                    
                </Space>
                <Space direction='vertical' style={{width:300}}>
                    <Alert
                    banner
                    message={
                        <Marquee pauseOnHover gradient={false} >
                            I can be a React component, multiple React components, or just some text.
                        </Marquee>
                    }
                    />

                    <Alert
                    message="Error"
                    description="这是一个错误的提示"
                    closable
                    action={
                        <Space direction="vertical">
                        <Button size="small" type="primary">
                            Accept
                        </Button>
                        <Button size="small" danger ghost>
                            Decline
                        </Button>
                        </Space>
                    }
                    />
                </Space>
            </Space>
            
        </Card>
    )
}

/**
 * Drawer 抽屉 | Message 全局提示
 */
const DrawerWidget=()=>{
    const [open,setOpen] = useState(false)
    const [placement,setPlacement] = useState("right")

    const showDrawer=()=>{
        setOpen(true)
    }

    const onChange=(e)=>{
        setPlacement(e.target.value)
    }

    const onClose =()=>{
        setOpen(false)
    }

    // ----Message-----

    const [messageApi,contextHolder] = message.useMessage()
    const key = "updatable"
    const openLoadingMessage=()=>{
        messageApi.open({
            key,
            type:"loading",
            content:"加载中...."
        })
        setTimeout(()=>{
            messageApi.open({
                key,
                type:"success",
                content:"加载完毕啦！",
                duration:2
            })
        },1000)
    }

    const openMessage=()=>{
        messageApi.open({
            type:"warning",
            content:"这是一个警告信息！！！",
            duration:3
        })
    }

    return(
        <Card title="Drawer 抽屉 | Message 全局提示">
            <Space>
                <Space direction='vertical'>
                    <Radio.Group value={placement} onChange={onChange}>
                        <Radio value={"top"}>Top</Radio>
                        <Radio value="right">right</Radio>
                        <Radio value="bottom">bottom</Radio>
                        <Radio value="left">left</Radio>
                    </Radio.Group>
                    <Button type="primary" onClick={showDrawer}>打开抽屉</Button>

                    <Drawer
                    title="Drawer with extra actions"
                    placement={placement}
                    width={500}
                    open={open}
                    onClose={onClose}
                    extra={
                        <Space>
                            <Button onClick={onClose}>Cancel</Button>
                            <Button type="primary" onClick={onClose}>OK</Button>
                        </Space>
                    }
                    >
                        <p>Some contents...</p>
                        <p>Some contents...</p>
                        <p>Some contents...</p>
                    </Drawer>
                </Space>
                <Divider type={"vertical"} style={{height:100}}/>
                <Space direction='vertical'>
                    {contextHolder}
                    <Button type='primary' onClick={openLoadingMessage}>打开加载提示messageBox</Button>
                    <Button type='primary' onClick={openMessage}>打开messageBox</Button>
                </Space>
            </Space>
            
        </Card>
    )
}

/**
 * Modal 对话框
 */
const ModalWidget=()=>{
    const [isModalOpen,setIsModalOpen] = useState(false)
    const showModal=()=>{
        setIsModalOpen(true)
    }
    const handleOk=()=>{
        setIsModalOpen(false)
    }
    const handleCancel=()=>{
        setIsModalOpen(false)
    }

    //----------------

    const ReachableContext = createContext(null);
    const UnreachableContext = createContext(null);
    const config = {
        title:"Use Hook",
        content:(
            <>
                <ReachableContext.Consumer>{(name) => `Reachable: ${name}!`}</ReachableContext.Consumer>
                <br />
                <UnreachableContext.Consumer>{(name) => `Unreachable: ${name}!`}</UnreachableContext.Consumer>
            </>
        )
    }
    const [modal,contextHolder] = Modal.useModal();

    //----------------用Modal中的confirm来弹出对话框

    const {confirm} = Modal
    // 普通对话框
    const showConfirm = () =>{
        confirm({
            title:"你想删点东西吗？",
            icon:<ExclamationCircleFilled />,
            content:"Some descriptions",
            onOk(){
                message.success("OK")
            },
            onCancel(){
                message.error("Cancel")
            }
        })
    }

    // 有延时对话框
    const showPromiseConfirm=()=>{
        confirm({
            title: 'Do you want to delete these items?',
            icon: <ExclamationCircleFilled />,
            content: 'When clicked the OK button, this dialog will be closed after 1 second',
            onOk(){
                return new Promise((resolve,reject)=>{
                    setTimeout(Math.random()>0.5?resolve:reject,1000)
                }).catch(()=>message.error("Oops Errors!"))
            },
            onCancel(){}
        })
    }

    // 自定义对话框按键文案
    const showDeleteConfirm=()=>{
        confirm({
            title: 'Are you sure delete this task?',
            icon: <ExclamationCircleFilled />,
            content: 'Some descriptions',
            okText:"确定",
            okType:"danger",
            cancelText:"不",
            onOk(){
                message.success("OK")
            },
            onCancel(){
                message.error("Cancel")
            }
        })
    }

    //  控制按键状态和样式
    const showPropsConfirm=()=>{
        confirm({
            title: 'Are you sure delete this task?',
            icon: <ExclamationCircleFilled />,
            content: 'Some descriptions',
            okText: 'Yes',
            okType: 'danger',
            okButtonProps:{
                disabled:true
            },
            cancelText:"NO",
            onOk(){
                message.success("OK")
            },
            onCancel(){
                message.error("Cancel")
            }
        })
    }

    //----------------利用Modal.error info 来弹出对话框
    const info = () => {
        Modal.error({
          title: 'This is a notification message',
          content: (
            <div>
              <p>some messages...some messages...</p>
              <p>some messages...some messages...</p>
            </div>
          ),
          onOk() {},
        });
      };

    return(
        <Card title="Modal 对话框">
            <Space direction='vertical'>
                <Button type='primary' onClick={showModal}>显示普通对话框</Button>
                <Modal
                    title="基础对话框"
                    open={isModalOpen}
                    onCancel={handleCancel}
                    onOk={handleOk}
                >
                    <p>Some contents...</p>
                    <p>Some contents...</p>
                    <p>Some contents...</p>
                </Modal>

                {/* ----------------------------- */}
                <ReachableContext.Provider value={"hahaha"}>
                    <Space>
                        <Button
                            type='primary'
                            onClick={async ()=>{
                                const confirmed = await modal.confirm(config)
                                console.log("ah confirmed:",confirmed)
                            }}  
                        >
                            Confirm
                        </Button>
                        <Button type={"dashed"} onClick={()=>modal.warning(config)}>Warning</Button>
                        <Button type={"default"} onClick={()=>modal.info(config)}>Info</Button>
                        <Button type={"primary"} danger onClick={()=>modal.error(config)}>Error</Button>
                        <UnreachableContext.Provider value="Bamboo" />
                        {contextHolder}
                    </Space>
                </ReachableContext.Provider>

                {/* ----------------------------- */}
                
                <Space>
                    <Button onClick={showConfirm}>Confirm</Button>
                    <Button onClick={showPromiseConfirm}>With promise</Button>
                    <Button onClick={showDeleteConfirm} type="dashed">Delete</Button>
                    <Button onClick={showPropsConfirm} type="dashed">With extra props</Button>
                    <Button onClick={info} type="primary">另外一种弹窗方式</Button>
                </Space>

            </Space>
        </Card>
    )
}

/**
 * Notification 通知提醒框 AND Popconfirm 气泡确认框
 */
const NotificationWidget=()=>{
    const [api,contextHolder] = notification.useNotification()
    const openErrorNotification =()=>{
        api.error({
            message:"我是错误通知信息",
            description:"我是错误描述信息~~~",
        })
    }

    const openWarningNotification =()=>{
        api.warning({
            message:"我是警告通知信息",
            description:"我是警告描述信息~~~",
            placement:"topLeft"
        })
    }

    const openNotification = () => {
        notification.open({
          message: '静态方法弹出的通知',
          description:
            'This is the content of the notification. This is the content of the notification. This is the content of the notification.',
          onClick: () => {
            console.log('Notification Clicked!');
          },
        });
      };

    return(
        <Card title="Notification 通知提醒框 AND Popconfirm 气泡确认框 ">
            <Space>
                {contextHolder}
                <Space direction='vertical'>
                    <Button type={"primary"} onClick={openErrorNotification} danger>显示错误信息通知</Button>
                    <Button type={"dashed"} onClick={openWarningNotification} warning>左上显示警告信息通知</Button>
                    <Button type={"link"} onClick={openNotification} warning>静态方法弹出通知</Button>
                </Space>

                <Space direction='vertical'>
                    <Popconfirm
                    title="Delete the task"
                    description="Are you sure to delete this task?"
                    onConfirm={()=>message.success("Confirm")}
                    onCancel={()=>message.error("Cancel")}
                    okText="YES"
                    cancelText="NO"
                    >
                        <Button danger>Delete</Button>
                    </Popconfirm>
                </Space>
            </Space>
        </Card>
    )
}

/**
 * Progress 进度条
 */
const ProgressWidget=()=>{
    const conicColors = {
        '0%': '#87d068',
        '50%': '#ffe58f',
        '100%': '#ffccc7',
      };
    const [percent,setPsercent]=useState(0)
    const increase = () =>{
        setPsercent((prev)=>{
            const newPercent = prev+10
            if(newPercent>100){
                return 100
            }
            return newPercent
        })
    }

    const decline =()=>{
        setPsercent((prev)=>{
            const newPercent = prev - 10
            if(newPercent<0){
                return 0
            }
            return newPercent
        })
    }
    return(
        <Card title="Progress 进度条">
            <Space direction='horizontal'>
                <Space direction='vertical'>
                    <Progress percent={30}/>
                    <Progress percent={50} status='active'/>
                    <Progress percent={80} status={"exception"}/>
                    <Progress percent={70} type={"circle"} status='active'/>
                    <Progress percent={70} type={"dashboard"} status='active'/>
                </Space>
                <Divider type='vertical' style={{height:200,margin:"0 30px"}}/>
                <Space direction='vertical'>
                    <Flex align='center' gap={"small"}>
                        <Progress
                            type='circle'
                            trailColor='#e6f4ff'
                            percent={40}
                            strokeWidth={20}
                            size={20}
                            format={(num)=>`进行中，已完成${num}%`}
                        />
                        <span>挣钱进度</span>
                    </Flex>
                    <Progress percent={95} type={"circle"} status='active' strokeColor={conicColors}/>
                    <Progress percent={percent} type='line'/>
                    <Progress percent={percent} type='circle'/>
                    <Button.Group>
                        <Button onClick={decline} icon={<MinusOutlined />} />
                        <Button onClick={increase} icon={<PlusOutlined />} />
                    </Button.Group>
                </Space>
                <Divider type='vertical' style={{height:200,margin:"0 30px"}}/>
                <Space direction='vertical'>
                    <Progress percent={30} steps={5} />
                    <Progress percent={100} steps={5} size="small" strokeColor={green[6]} />
                    <Progress percent={60} steps={5} strokeColor={[green[6], green[6], red[5]]} />
                </Space>
                
            </Space>
            

        </Card>
    )
}

/**
 * Watermar 水印 AND Result 结果
 */
const WatermarkWidget=()=>{
    return(
        <Card title="Watermar 水印">
            <Watermark content={["Cooper","Ant Design"]}>
                <Result
                    status={"500"}
                    title="500"
                    subTitle="Sorry, you are not authorized to access this page."
                    extra={<Button type="primary">Back Home</Button>}
                />
                <Result
                    status={"403"}
                    title="403"
                    subTitle="Sorry, you are not authorized to access this page."
                    extra={<Button type="primary">Back Home</Button>}
                />
            </Watermark>
        </Card>
    )
}



const FeedbackWidgetOne =()=>{
    return(
      <Space direction='vertical' style={{width:"100%"}}>
        <AlertWidget/>
        <DrawerWidget/>
        <ModalWidget/>
        <NotificationWidget/>
        <ProgressWidget/>
        <WatermarkWidget/>
      </Space>
    )
}

export default FeedbackWidgetOne