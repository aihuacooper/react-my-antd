import {Breadcrumb, Button, Card, Flex, Layout,Menu,MenuProps, Slider, Space ,Tooltip} from "antd"
import { LaptopOutlined, NotificationOutlined, UserOutlined,LikeOutlined,CommentOutlined,StarOutlined } from '@ant-design/icons';
import React, { useState } from 'react';
import { Content, Header} from "antd/es/layout/layout";
import styles from "./index.module.less"
import Sider from "antd/es/layout/Sider";

/**
 * 1.定义属性类名className嵌套时，外层父组件有类名，子类组件的类名才嵌套，否则定义子类类名以及内部属性不起作用。
 * 2.overflow 取值有auto、visible、hidden、scroll。其中auto表示当内容超出元素框时，显示滚动条滚动查看，只有在需要的时候显示滚动条。
 */

const logoUrl = "http://passport-uat.ppts.xueda.com/PPTSWebApp/PPTSPassportService/Content/login/images/logo.png"

const headItems = new Array(5).fill(null).map((_,index)=>{
    return{
        key:index+1,
        label:`课程${index}`
    }
})

const siderItems = [UserOutlined, LaptopOutlined, NotificationOutlined].map((icon,index)=>{
    const key = String(index+1)
    return {
        key:`sub${key}`,
        icon:React.createElement(icon),
        label:`课程内容${key}`,
        children:new Array(4).fill(null).map((_,j)=>{
            const subKey =index * 4 + j + 1;
            return {
                key: subKey,
                label: `语文${subKey}`,
            };
        }),
    }
})


const LayoutTwo =()=>{

    const [value,changeValue] = useState(0)

    return(
        <Layout className={styles.layoutFather}>
            <Header className={styles.headerStyle}>
                <img src={logoUrl} alt="gaga" width={100} />
                <Menu className={styles.horizontalMenuStyle}
                    theme="dark"
                    mode="horizontal"
                    items={headItems}
                />
            </Header>
            <Layout className={styles.secondLayoutStyle} >
                <Sider className={styles.siderStyle}>
                    <Menu
                        theme="dark"
                        className={styles.verticalMenuStyle}
                        defaultSelectedKeys={["1"]}
                        mode="inline"
                        items={siderItems}
                    />
                </Sider>
                <Content className={styles.contentStyle}>
                    <Breadcrumb className={styles.bcStyle} items={[{key:"1",title:"语文"},{key:"2",title:"汉字"},{key:"3",title:"拼音"}]}/>
                    <Flex wrap  justify="space-round" align="flex-end">
                        {new Array(10).fill(null).map((_,index)=>{
                            return <Button className={styles.btStyle} type="primary">作业{index}</Button>
                        })}
                    </Flex>
                    
                    <Card title={`Slider标签 value = ${value}`} className={styles.cardStyle}>
                        <Slider value={value} onChange={changeValue}/>
                    </Card>

                    <Card title={`Space标签、Tooltip标签、Space.Compact标签演示`} className={styles.cardStyle}>
                    <Space size={"middle"}>
                        <Space.Compact>
                            <Tooltip title="Like">
                                <Button icon={<LikeOutlined />} />
                            </Tooltip>
                            <Tooltip title="Comment">
                                <Button icon={<CommentOutlined />} />
                            </Tooltip>
                            <Tooltip title="Star">
                                <Button icon={<StarOutlined />} />
                            </Tooltip>
                        </Space.Compact>
                        
                        <Space.Compact>
                            <Button type="primary">英语</Button>
                            <Button type="primary">语文</Button>
                            <Button type="primary">数学</Button>
                        </Space.Compact>
                    
                        <Space.Compact direction="vertical">
                            <Button type="primary">物理</Button>
                            <Button type="primary">化学</Button>
                            <Button type="primary">地理</Button>
                        </Space.Compact>
                    </Space>
                    </Card>

                    
                </Content>
            </Layout>
        </Layout>
    )
}

export default LayoutTwo