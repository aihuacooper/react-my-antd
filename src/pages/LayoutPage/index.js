import {Button, Flex} from "antd"
import { Outlet } from "react-router-dom"

const LayoutPage =()=>{
    return(
        <Outlet/>
    )
}

export default LayoutPage