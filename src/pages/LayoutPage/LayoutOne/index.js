import { Breadcrumb, Layout, Menu, theme,Image } from 'antd';
import { Header,Content, Footer } from 'antd/es/layout/layout';
import styles from "./index.module.less"

/**
 * 1.拼接字符串用反向字符串来包裹，用${}来承接变量。
 * 2.可以使用new Array(15).fill(null).map来创建一个固定长度的，自定义item的列表。
 */
const items = new Array(15).fill(null).map((_,index)=>{
    return{
        key:index+1,
        label:`科目 ${index+1}` // 拼接字符串用反向字符串来包裹，用${}来承接变量
    }
})


const logoUrl = "http://passport-uat.ppts.xueda.com/PPTSWebApp/PPTSPassportService/Content/login/images/logo.png"

const LayoutOne =()=>{
    console.log(items)
    return(
        <Layout>
            <Header className={styles.headerStyle}>
                <img src={logoUrl} alt='logo' width={100}/>
                <Menu 
                    theme='dark'
                    mode='horizontal'
                    defaultSelectedKeys={["2"]}
                    items={items}
                    className={styles.menuStyle}
                />
            </Header>

            <Content className={styles.contentStyle}>
                <Breadcrumb className={styles.bcStyle} separator=">" items={[{title:"主页"},{title:"第二页"},{title:"第三页"}]}/>
                <div className={styles.divContentStyle}>
                    Cotent
                </div>
            </Content>

            <Footer className={styles.footerStyle}>
                 Ant Design ©{new Date().getFullYear()} Created by AH
            </Footer>
        </Layout>
    )
}

export default LayoutOne