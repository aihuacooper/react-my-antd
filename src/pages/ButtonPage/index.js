import { Button, Flex, Tooltip } from 'antd';
import { SearchOutlined } from '@ant-design/icons';
import styles from "./index.module.less"
const ButtonPage =()=>{
    return(
        <div>
            <Flex wrap gap="small" className={styles.flexLayout}>
            <Button type={'primary'}>按键</Button>
            <Button loading={true} type={'primary'}>按键</Button>
            <Tooltip title="gagag">
                <Button className={styles.btCircle} type='primary' shape='circle' size='large'>按键</Button>
            </Tooltip>
            <Button type="dashed">Dashed Button</Button>
            <Button type="text">Text Button</Button>
            <Button type="link">Link Button</Button>

            <Tooltip title="search">
                <Button type="primary" shape="circle" icon={<SearchOutlined />} />
            </Tooltip>
            <Button type="primary" shape="circle">
                A
            </Button>
            <Button type="primary" icon={<SearchOutlined />}>
                Search
            </Button>
            <Tooltip title="search">
                <Button shape="circle" icon={<SearchOutlined />} />
            </Tooltip>
            <Button icon={<SearchOutlined />}>Search</Button>
            <Button disabled>不可用</Button>
            <Button type='primary' href='https://www.baidu.com'>跳转到百度</Button>
            <Button danger type='primary'>危险按键</Button>
            <Button type='primary' shape='round'>圆角按键</Button>
            
        </Flex>
        <Flex className={styles.flexLayoutTwo} vertical>
            <Button type='primary' shape='round' block>与父组件一样的宽度</Button>
        </Flex>
        </div>
    )
}

export default ButtonPage