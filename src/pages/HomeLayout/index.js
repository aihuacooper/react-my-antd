import React, { useState } from 'react';
import {
  DesktopOutlined,
  FileOutlined,
  PieChartOutlined,
  TeamOutlined,
  UserOutlined,
  AccountBookFilled,
  AppstoreFilled,
  ApartmentOutlined,
  AppstoreAddOutlined,
  TableOutlined,
  ProductOutlined,
  UngroupOutlined,
  DeliveredProcedureOutlined,
  AimOutlined,
  NotificationOutlined,
  PaperClipOutlined,
  FieldTimeOutlined,
  BlockOutlined,
  CalculatorOutlined,
  CarOutlined,
  ControlOutlined,
  ShopOutlined,
  SmileOutlined,

} from '@ant-design/icons';
import { Breadcrumb, Layout, Menu, theme } from 'antd';
import styles from "./index.module.less"
import { Outlet, useNavigate } from 'react-router-dom';
import _ from "lodash"

const { Header, Content, Footer, Sider } = Layout;
function getItem(label, key, icon, children) {
  return {
    key,
    icon,
    children,
    label,
  };
}
const items = [
  getItem('Button', '1', <PieChartOutlined />),
  getItem('Flex', '2', <DesktopOutlined />,[
    getItem("Flex基本布局","21",<AccountBookFilled />,),
    getItem("对齐方式","22",<AppstoreFilled />,)
  ]),
  getItem('Grid', '3', <TableOutlined />,[
    getItem("Grid基本布局","31",<AppstoreAddOutlined />,),
  ]),
  getItem('Layout', '4', <ApartmentOutlined />,[
    getItem("Layout布局一","41",<ProductOutlined />,),
    getItem("Layout布局二","42",<UngroupOutlined />,)
  ]),
  getItem('导航组件', '5', <DeliveredProcedureOutlined />,[
    getItem("导航组件一","51",<AimOutlined />,),
  ]),
  getItem('数据录入组件', '6',<NotificationOutlined />,[
    getItem("数据录入组件一","61",<PaperClipOutlined />,),
    getItem("数据录入组件二","62",<FieldTimeOutlined />,),
    getItem("数据录入组件三","63",<BlockOutlined />,),
  ]),
  getItem('数据展示组件', '7',<CalculatorOutlined />,[
    getItem("数据展示组件一","71",<CarOutlined />,),
    getItem("数据展示组件二","72",<ControlOutlined />,),
  ]),
  getItem('反馈组件', '8',<ShopOutlined />,[
    getItem("数据展示组件一","81",<SmileOutlined />,),
  ]),
];

const data = [
  {"key":1,"name":"ga"},
  {"key":2,"name":"gdda"},
  {"key":3,"name":"gsdfa"},
  {"key":4,"name":"ggg"},
  {"key":5,"name":"333"},
]

const HomeLayout = () => {
  const [collapsed, setCollapsed] = useState(false);
  const [seletedValue, setSeletedValue] = useState("Button");
  const {
    token: { colorBgContainer, borderRadiusLG },
  } = theme.useToken();

  const navigate = useNavigate()

  function handleClick(e){
    const key = e.key; // 获取被点击菜单项的 key
    const selectedItem = findItemByKey(items, key); // 根据 key 查找对应的项
    if (selectedItem) {
      const label = selectedItem.label;
      setSeletedValue(label)
    }
    switch(e.key){
      case "1":
        navigate("/")
        break;
      case "21":
        navigate("flex-page/flex-base-page")
        break;
      case "22":
        navigate("flex-page/flex-align-page")
        break;
      case "31":
        navigate("grid-page/grid-base-page")
        break;
      case "41":
        navigate("layout-page/layout-page-one")
        break;
      case "42":
        navigate("layout-page/layout-page-two")
        break;
      case "51":
        navigate("nav-page/nav-page-one")
        break;
      case "61":
        navigate("data-input-page-one")
        break;
      case "62":
        navigate("data-input-page-two")
        break;
      case "63":
        navigate("data-input-page-three")
        break;
      case "71":
          navigate("data-show-page-one")
          break;
      case "72":
          navigate("data-show-page-two")
          break;
      case "81":
          navigate("feedback-page-one")
          break;
      default:
        alert("no page")
    }
    
  }

  // 根据 key 查找对应的项
  const findItemByKey = (data, key) => {
    for (let item of data) {
      if (item.key === key) {
        return item;
      }
      if (item.children) {
        const found = findItemByKey(item.children, key);
        if (found) return found;
      }
    }
    return null;
  };

  return (
    <Layout className={styles.homeLayout}>
      <Sider collapsible collapsed={collapsed} onCollapse={(value) => setCollapsed(value)}>
        <div className="demo-logo-vertical" />
        <Menu theme="dark" defaultSelectedKeys={['1']} mode="inline" items={items} onClick={handleClick}/>
      </Sider>
      <Layout>
        <Header
          style={{
            padding: 0,
            background: colorBgContainer,
          }}
        >
          <p className={styles.pText}>Ant Design Test - {seletedValue}</p>
        </Header>
        <Content
          style={{
            margin: '0 16px',
            overflow:"auto"
          }}
        >
          <div
            style={{
              padding: 24,
              minHeight: 360,
              background: colorBgContainer,
              borderRadius: borderRadiusLG,
            }}
          >
            <Outlet/>
          </div>
        </Content>
        <Footer
          style={{
            textAlign: 'center',
          }}
        >
          Ant Design ©{new Date().getFullYear()} Created by Ant UED
        </Footer>
      </Layout>
    </Layout>
  );
};

export default HomeLayout