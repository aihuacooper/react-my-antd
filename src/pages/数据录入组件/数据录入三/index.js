import { Cascader, Form, Button, Card,Space,Image,Row,Col, AutoComplete, Checkbox, DatePicker, TimePicker, Select,Radio,Tag, Input,Typography, InputNumber, Switch, Upload, message } from 'antd';
import styles from "./index.module.less"
import { DownOutlined ,UpOutlined} from '@ant-design/icons';
import { LockOutlined, UserOutlined } from '@ant-design/icons';
import { FrownOutlined, MehOutlined, UploadOutlined,PlusOutlined } from '@ant-design/icons';
import { useEffect, useState } from "react";
import { FOCUSABLE_SELECTOR } from '@testing-library/user-event/dist/utils';
const { Option } = Select;

/**
 * 表单录入
 */
const FormBasicWidget =()=>{
    const onFinish = (values) => {
    console.log('Success:', values);
    };
    const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
    };
    return(
        <Card title="From 基本使用">
                <Form
                name={"basic"}
                labelCol={{span:4}}
                wrapperCol={{span:20}}
                style={{width:400}}
                initialValues={{
                    username:"gagag",
                    password:"1234",
                    remember:true
                }}
                onFinish={onFinish}
                onFinishFailed={onFinishFailed}
                autoComplete="off"
                >
                    <Form.Item
                    label={"名字"}
                    name={"username"}
                    rules={[
                        {
                            required:true,
                            message:"请输入你的名字"
                        }
                    ]}
                    >
                        <Input/>
                    </Form.Item>

                    <Form.Item
                    name={"password"}
                    label={"密码"}
                    rules={[
                        {
                            required:true,
                            message:"请输入密码"
                        }
                    ]}
                    >
                        <Input.Password/>
                    </Form.Item>

                    <Form.Item
                    name={"remember"}
                    valuePropName='checked'
                    wrapperCol={{
                        offset:4,
                        span:20
                    }}
                    >
                        <Checkbox>Remember me</Checkbox>
                    </Form.Item>

                    <Form.Item
                    wrapperCol={{
                        offset:10,
                        span:14
                    }}
                    >
                        <Button type='primary' htmlType="submit">提交</Button>
                    </Form.Item>

                </Form>
            </Card>
    )
}

/**
 * 表单数据调用
 */
const FormDataWidget =()=>{
    const layout = {
        labelCol:{
            span:8
        },
        wrapperCol:{
            span:16
        }
    }
    const tailLayout={
        wrapperCol:{
            offset:8,
            span:16
        }
    }

    const [form] = Form.useForm();
    const onGenderChange =(value)=>{
        switch(value){
            case "male":
                form.setFieldValue("note","Hi,man!");
                break;
            case "female":
                form.setFieldValue("note","Hi,lady!");
                break;
            case "other":
                form.setFieldValue("note","HI SB!");
                break;
            default:
        }
    }

    const onFinish=(values)=>{
        message.info(values.gender)
        console.log(values)
    }

    const onReset=()=>{
        form.resetFields();
    }

    const onFill=()=>{
        form.setFieldsValue({
            note:"Hello World!",
            gender:"hahaha"
        })
    }

    return(
        <Card title="From 数据调用">
            <Form
            {...layout}
            form={form}
            name='control-hooks'
            onFinish={onFinish}
            style={{
                width:400
            }}
            >
                <Form.Item
                name={"note"}
                label="Note"
                rules={[
                    {
                        required:true,
                        message:"填写点东西吧！"
                    }
                ]}
                >
                    <Input/>
                </Form.Item>

                <Form.Item
                name={"gender"}
                label="Gender"
                rules={[
                    {
                        required:true,
                        message:"不许不填写内容！"
                    }
                ]}
                >
                    <Select
                    placeholder="请选择一个选项"
                    onChange={onGenderChange}
                    allowClear
                    >
                        <Option value="male">男人</Option>
                        <Option value="female">女人</Option>
                        <Option value="other">其他</Option>
                    </Select>
                </Form.Item>

                <Form.Item
                noStyle
                shouldUpdate={
                    (prevValues,currnetValues)=>prevValues.gender !== currnetValues.gender
                }
                >
                    {
                        ({getFieldValue})=>
                            getFieldValue("gender") === "other"?(
                                <Form.Item
                                name={"customizeGender"}
                                label="Customize Gender"
                                rules={[
                                    {
                                        required:true,
                                        message:"你要干哈！"
                                    }
                                ]}
                                >
                                    <Input/>
                                </Form.Item>
                            ):null
                    }

                </Form.Item>

                <Form.Item {...tailLayout}>
                    <Space>
                        <Button type='primary' htmlType='submit'>提交</Button>
                        <Button type='primary' onClick={onReset}>重置</Button>
                        <Button type='primary' onClick={onFill}>都填充了</Button>
                    </Space>
                </Form.Item>

            </Form>
        </Card>
    )
}

/**
 * 表单布局改变
 */
const FormLayoutWidget=()=>{
    const [form] = Form.useForm()
    const [formLayout,setFormLayout] = useState("horizontal")
    const onFormLayoutChange = ({layout})=>{
        setFormLayout(layout)
    }
    const formItemLayout = formLayout === "horizontal"?
        {
            labelCol:{
                span:4
            },
            wrapperCol:{
                span:14
            }
        }
        :null;
    const buttonItemLayout = formLayout === "horizontal"?
        {
            wrapperCol:{
                span:14,
                offset:4
            }
        }
        :null;

    // 数据监听
    const msgValueA = Form.useWatch("msgA",form)
    const msgValueB = Form.useWatch((value)=>{
        return value.msgB
    },form)

    return(
        <Card title="From 布局 & rules & Form.useWatch">
            <Form
            {...formItemLayout}
            layout={formLayout}
            form={form}
            disabled={false} 
            initialValues={{
                layout:formLayout,
                msgA:"",
                msgB:"",
            }}
            labelWrap
            onValuesChange={onFormLayoutChange}
            style={{
                width:formLayout === "inline"?"none":500
            }}
            >
                <Form.Item
                label="Form  label换行"
                name={"layout"}
                >
                    <Radio.Group value={formLayout}>
                        <Radio.Button value={"horizontal"}>Horizontal</Radio.Button>
                        <Radio.Button value={"vertical"}>Vertical</Radio.Button>
                        <Radio.Button value={"inline"}>Inline</Radio.Button>
                    </Radio.Group>
                </Form.Item>

                <Form.Item
                label="Field A"
                name={"msgA"}
                rules={[
                    {
                        required:true,
                        message:"填写点东西吧！"
                    },
                    {
                        type:"email"
                    }
                ]}
                >
                    <Input placeholder='请输入A'/>
                </Form.Item>

                <Form.Item
                name={"msgB"}
                label="Field B"
                rules={[
                    {
                        type:"url",
                        warningOnly:true
                    }
                ]}
                hasFeedback
                validateDebounce={1000}
                >
                    <Input placeholder='请输入B'/>
                </Form.Item>

                <Form.Item
                label="观察的数据"
                >
                    <p>msgValueA:{msgValueA}   msgValueB.length:{msgValueB}</p>
                </Form.Item>

                <Form.Item
                {...buttonItemLayout}
                >
                    <Button type='primary' htmlType='submit'>提交</Button>
                </Form.Item>
            </Form>
        </Card>
    )
}


const SubmitButton =({form,children})=>{
    const [submittable,setSubmittable] = useState(false)
    const values = Form.useWatch([],form)
    useEffect(()=>{
        form.validateFields({
            validateOnly:true
        })
        .then(()=>setSubmittable(true))
        .catch(()=>setSubmittable(false))
    },[form,values])
    return(
        <Button type='primary' htmlType='submit' disabled={!submittable}>{children}</Button>
    )
}

/**
 * 根据表单数据改变按键状态
 */
const FormButtonDisableWidget=()=>{
    const [form] = Form.useForm()
    return(
        <Card title="Form 按键disable状态">
            <Form
            form={form}
            name='validateOnl'
            layout='vertical'
            autoComplete='off'
            style={{
                width:300
            }}
            >
                <Form.Item
                name={"name"}
                label="Name"
                rules={[
                    {
                        required:true
                    }
                ]}
                >
                    <Input/>
                </Form.Item>

                <Form.Item
                name={"age"}
                label="Age"
                rules={[
                    {
                        required:true
                    }
                ]}
                >
                    <Input/>
                </Form.Item>

                <Form.Item>
                    <Space>
                        <SubmitButton form={form}>提交</SubmitButton>
                        <Button htmlType='reset'>重置</Button>
                    </Space>
                </Form.Item>
            </Form>
        </Card>
    )
}

/**
 * 登录
 */
const FormLoginWidget=()=>{
    const onFinish=(values)=>{
        const value = values.username+" === "+values.password
        message.info(value)
    }
    const onFinishFailed=(errorInfo)=>{
        console.log('Failed:', errorInfo);
    }
    return(
        <Card title="Form 登录">
            <Form
            name='normal_log'
            className='login-form'
            style={{
                width:400
            }}
            initialValues={{
                remember:true
            }}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            >
                <Form.Item
                name={"username"}
                rules={[
                    {
                        required:true,
                        message:"请输入名字"
                    }

                ]}
                >
                    <Input prefix={<UserOutlined/>} placeholder='Username'/>
                </Form.Item>

                <Form.Item
                name={"password"}
                rules={[
                    {
                        required:true,
                        message:"请输入密码"
                    }
                ]}

                >
                    <Input.Password
                    prefix={<LockOutlined/>}
                    type='password'
                    placeholder='Password'
                    />
                </Form.Item>

                <Form.Item>
                    <Form.Item
                    name={"remember"}
                    valuePropName='checked'
                    noStyle
                    >
                        <Checkbox>Remember me</Checkbox>
                    </Form.Item>

                    <a className="login-form-forgot" href="">Forgot Password</a>
                </Form.Item>

                <Form.Item>
                    <Button type="primary" htmlType="submit" className="login-form-button">
                    Log in
                    </Button>
                    Or <a href="">register now!</a>
                </Form.Item>
            </Form>
        </Card>
    )
}

/**
 * 注册
 */
const RegisterWidget=()=>{
    const residences = [
        {
            value:"zhejiang",
            label:"浙江",
            children:[
                {
                    value:"hangzhou",
                    label:"杭州",
                    children:[
                        {
                            value:"xihua",
                            label:"西湖"
                        }
                    ]
                }

            ]
        },
        {
            value:"jiangsu",
            label:"江苏",
            children:[
                {
                    value:"nanjing",
                    label:"南京",
                    children:[
                        {
                            value:"zhonghuamen",
                            label:"中华门"
                        }
                    ]
                }
            ]
        }
    ]

    const [form] = Form.useForm()
    const onFinish=(values)=>{
        message.info("gaga")
    }
    const prefixSelector = (
        <Form.Item
        name={"prefix"}
        noStyle
        >
            <Select
            style={{
                width:70
            }}
            >
                <Option value={"+86"}>+86</Option>
                <Option value={"+87"}>+87</Option>
            </Select>
        </Form.Item>
    )

    const suffixSelector = (
        <Form.Item
        name={"suffix"}
        noStyle
        >
            <Select
            style={{
                width:70
            }}
            >
                <Option value="USD">$</Option>
                <Option value="CNY">¥</Option>
            </Select>

        </Form.Item>
    )

    const [autoCompleteResult,setAutoCompleteResult] = useState([])
    const onWebsiteChange = (value) =>{
        if(!value){
            setAutoCompleteResult([])
        }else{
            setAutoCompleteResult([".com",".org",".net"].map((domain)=>`${value}${domain}`))
        }
    }
    const websiteOptions = autoCompleteResult.map((website)=>({
        label:website,
        value:website
    }))

    return(
        <Card title="Form 注册">
            <Form
            {...formItemLayout}
            form={form}
            name='register'
            onFinish={onFinish}
            initialValues={{
                residences:["zhejiang","hangzhou","xihu"],
                prefix:"86"
            }}
            style={{
                width:400
            }}
            scrollToFirstError
            >
                
                <Form.Item
                name={"email"}
                label="邮箱"
                rules={[
                    {
                        type:"email",
                        message:"输入的不是邮箱格式！"
                    },
                    {
                        required:true,
                        message:"请输入邮箱地址！"
                    }
                ]}
                >
                    <Input/>
                </Form.Item>

                <Form.Item
                name={"password"}
                label="密码"
                rules={[
                    {
                        required:true,
                        message:"请输入密码！"
                    }
                ]}
                hasFeedback
                >
                    <Input.Password/>
                </Form.Item>

                <Form.Item
                name={"comfirm"}
                label="密码确认"
                dependencies={["password"]}
                hasFeedback
                rules={[
                    {
                        required:true,
                        message:"请确认你的密码"
                    },
                    ({getFieldValue}) =>({
                        validator(_,value){
                            if(!value || getFieldValue("password") === value){
                                return Promise.resolve();
                            }
                            return Promise.reject(new Error("与刚才输入的密码不匹配！"))
                        }
                    })
                ]}
                >
                    <Input.Password/>
                </Form.Item>

                <Form.Item
                name={"nickname"}
                label="昵称"
                tooltip="你想让别人怎么称呼"
                rules={[
                    {
                        required:true,
                        message:"请输入你的昵称",
                        whitespace:true
                    }
                ]}
                >
                    <Input/>
                </Form.Item>

                <Form.Item
                name={"residence"}
                label="常住地址"
                rules={[
                    {
                        type:"array",
                        required:true,
                        message:"请输入你的常住地址！"
                    }
                ]}
                >
                    <Cascader options={residences}/>
                </Form.Item>

                <Form.Item
                name={"phone"}
                label="手机号"
                rules={[
                    {
                        required:true,
                        message:"请输入你的手机号！"
                    }
                ]}
                >
                    <Input
                    addonBefore={prefixSelector}
                    style={{
                        width:"100%"
                    }}
                    />
                </Form.Item>

                <Form.Item
                name={"donation"}
                label="捐赠多少钱"
                rules={[
                    {
                        required:true,
                        message:"请输入你要掏的钱！"
                    }
                ]}
                >
                    <InputNumber
                    addonAfter={suffixSelector}
                    style={{
                        width:"100%"
                    }}
                    />
                </Form.Item>

                <Form.Item
                name={"website"}
                label="网址"
                >
                    <AutoComplete
                    options={websiteOptions}
                    onChange={onWebsiteChange}
                    placeholder="website"
                    >
                        <Input/>
                    </AutoComplete>
                </Form.Item>

                <Form.Item
                name={"intro"}
                label="自我介绍"
                rules={[
                    {
                        required:true,
                        message:"介绍一下自己啊！"
                    }
                ]}
                >
                    <Input.TextArea showCount maxLength={100}/>
                </Form.Item>

                <Form.Item
                name={"gender"}
                label="性别"
                rules={[
                    {
                        required:true,
                        message:"请选择一个性别啊！"
                    }
                ]}
                >
                    <Select
                    placeholder="选择一个性别！"
                    >
                        <Option value="male">神族</Option>
                        <Option value="female">虫族</Option>
                        <Option value="other">人族</Option>
                    </Select>
                </Form.Item>

                <Form.Item
                label="验证码"
                extra="我们必须确定你是一个人！"
                >
                    <Row gutter={8}>
                        <Col span={12}>
                            <Form.Item
                            name={"captcha"}
                            noStyle
                            rules={[
                                {
                                    required:true,
                                    message:"请输入你的验证码！"
                                }
                            ]}
                            >
                                <Input/>
                            </Form.Item>
                        </Col>
                        <Col span={12}>
                            <Button type='primary'>获取验证码</Button>
                        </Col>
                    </Row>
                </Form.Item>

                <Form.Item
                name={"agreement"}
                valuePropName='checked'
                rules={[
                    {
                        validator:(_,value)=>{
                            return value?Promise.resolve():Promise.reject(new Error("请先同意协议！！"))
                        }
                    }
                ]}
                {...tailFormItemLayout}
                >
                    <Checkbox>
                        我已经阅读<a href='https://www.baidu.com'>隐私协议</a>
                    </Checkbox>
                </Form.Item>

                <Form.Item
                {...tailFormItemLayout}
                >
                    <Button type='primary' htmlType='submit'>注册会员</Button>
                </Form.Item>

            </Form>
        </Card>
    )
}

const formItemLayout = {
    labelCol: {
      xs: {
        span: 24,
      },
      sm: {
        span: 8,
      },
    },
    wrapperCol: {
      xs: {
        span: 24,
      },
      sm: {
        span: 16,
      },
    },
  };

  const tailFormItemLayout = {
    wrapperCol: {
      xs: {
        span: 24,
        offset: 0,
      },
      sm: {
        span: 16,
        offset: 8,
      },
    },
  };

const DataInputPageThree =()=>{
    
    return(
        <Space direction="vertical">
            <Space>
                <FormBasicWidget/>
                <FormDataWidget/>
            </Space>
            <Space>
                <FormLayoutWidget/>
                <FormButtonDisableWidget/>
            </Space>
            <Space>
                <FormLoginWidget/>
            </Space>
            <Space>
                <RegisterWidget/>
            </Space>
        </Space>
    )
}

export default DataInputPageThree