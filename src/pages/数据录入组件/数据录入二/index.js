import { Cascader, Rate, Button, Card,Space,Image, AutoComplete, Checkbox, DatePicker, TimePicker, Select,Radio,Tag, Input,Typography, InputNumber, Switch, Upload, message } from 'antd';
import styles from "./index.module.less"
import { DownOutlined ,UpOutlined} from '@ant-design/icons';
import { FrownOutlined, MehOutlined, UploadOutlined,PlusOutlined } from '@ant-design/icons';
import { useState } from "react";
const { Option } = Select;
const {Search,TextArea} = Input
const { Title } = Typography;

/**
 * Input 输入框
 */
const InputWidget=()=>{

    const[content,setContent] = useState("gege")

    const selectBefore = (
        <Select defaultValue="http://">
          <Option value="http://">http://</Option>
          <Option value="https://">https://</Option>
        </Select>
      );
      const selectAfter = (
        <Select defaultValue=".com">
          <Option value=".com">.com</Option>
          <Option value=".jp">.jp</Option>
          <Option value=".cn">.cn</Option>
          <Option value=".org">.org</Option>
        </Select>
      );

      const onChange=(value)=>{
        setContent(value.target.value)
        console.log(value.target.value)
      }

      const onSearch =(value)=>{
        console.log(value);
      }

      const sharedProps = {
        onChange,
      };

    return(
        <Card title={"Input 输入框"}>
            <Space>
                <Space direction='vertical'>
                    <Input addonBefore={"http://"} addonAfter={".com"} defaultValue={"gaga"} value={content} onChange={onChange}/>
                    <Input addonBefore={selectBefore} addonAfter={selectAfter} defaultValue={"haha"}/>
                    <Search 
                        placeholder='请输入搜索内容' 
                        allowClear 
                        onSearch={onSearch} 
                        />
                    <Search 
                        placeholder='请输入搜索内容' 
                        allowClear 
                        enterButton={"搜索"}
                        onSearch={onSearch} 
                        />
                    <Search 
                        placeholder='请输入搜索内容' 
                        allowClear 
                        enterButton={"搜索"}
                        loading
                        onSearch={onSearch} 
                        />
                </Space>
                <Space direction='vertical'>
                    <TextArea 
                        defaultValue={"hahaha"} 
                        style={{
                            width:200,
                            height:100
                        }}
                        autoSize={{
                            minRows:2,
                            maxRows:3
                        }}
                        allowClear
                        showCount
                        />
                    <Title level={5}>请输入六位密码</Title>
                    <Input.OTP length={6}/>
                    <Input.OTP mask="🔒" {...sharedProps} />
                    <Input.OTP mask="@" {...sharedProps} />
                </Space>
                <Space direction='vertical'>
                    <Input.Password placeholder='请输入密码'/>
                    <Input.Password 
                        placeholder='请输入密码'
                        status={"error"}
                        />
                    <p>最大数字5 最小数字1</p>
                    <InputNumber min={1} max={5} defaultValue={2} onChange={onChange}/>
                </Space>
            </Space>
            
        </Card>
    )
}

/**
 * Switch开关与上传组件
 */
const SwitchUploadWidget=()=>{
    const onChange = (value) => {
        console.log('changed', value);
    };

    const props ={
        name:"file",
        action:"https://660d2bd96ddfa2943b33731c.mockapi.io/api/upload",
        headers: {
            authorization: 'authorization-text',
        },
        onChange(info){
            if(info.file.status !== "uploading"){
                console.log(info.file,info.fileList)
            }
            if(info.file.status === "done"){
                message.success(`${info.file.name}文件已经上传成功~`)
            }else if(info.file.status === "error"){
                message.error(`${info.file.name}文件已经失败！`)
            }
        }
    }

    const getBase64 = (file) =>
        new Promise((resolve, reject) => {
          const reader = new FileReader();
          reader.readAsDataURL(file);
          reader.onload = () => resolve(reader.result);
          reader.onerror = (error) => reject(error);
    });

    const [previewOpen, setPreviewOpen] = useState(false);
    const [previewImage, setPreviewImage] = useState('');

    const [fileList, setFileList] = useState([
        {
          uid: '-1',
          name: 'image.png',
          status: 'done',
          url: 'https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png',
        },
        {
          uid: '-xxx',
          percent: 50,
          name: 'image.png',
          status: 'uploading',
          url: 'https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png',
        },
        {
          uid: '-5',
          name: 'image.png',
          status: 'error',
        },
      ]);

      const handlePreview = async (file) => {
        if (!file.url && !file.preview) {
          file.preview = await getBase64(file.originFileObj);
        }
        setPreviewImage(file.url || file.preview);
        setPreviewOpen(true);
      };
      const handleChange = ({ fileList: newFileList }) => setFileList(newFileList);
      const uploadButton = (
        <button
          style={{
            border: 0,
            background: 'none',
          }}
          type="button"
        >
          <PlusOutlined />
          <div
            style={{
              marginTop: 8,
            }}
          >
            Upload
          </div>
        </button>
      );

    return(
        <Card title="Switch 开关 & Upload 上传">
            <Space>
                <Space direction='vertical'>
                    <Switch checkedChildren={"开启"} unCheckedChildren={"关闭"} defaultChecked onChange={onChange}/>
                    <Switch defaultChecked loading/>
                </Space>
                <Space direction="vertical">
                    <Upload {...props}>
                        <Button icon={<UploadOutlined />}>Click to Upload</Button>
                    </Upload>
                </Space>
                <Space>
                    <Upload
                        action="https://660d2bd96ddfa2943b33731c.mockapi.io/api/upload"
                        listType="picture-circle"
                        fileList={fileList}
                        onPreview={handlePreview}
                        onChange={handleChange}
                    >
                        {fileList.length >= 8 ? null : uploadButton}
                    </Upload>
                    {previewImage && (
                        <Image
                            wrapperStyle={{
                                display:"none"
                            }}
                            preview={{
                                visible:previewOpen,
                                onVisibleChange:(visible)=>setPreviewOpen(visible),
                                afterOpenChange:(visible)=>!visible && setPreviewImage("")
                            }}
                            src={previewImage}
                        />
                    )}
                </Space>
            </Space>
        </Card>
    )
}


const DataInputPageTwo =()=>{
    
    return(
        <Space direction="vertical">
            <InputWidget/>
            <SwitchUploadWidget/>
        </Space>
    )
}

export default DataInputPageTwo