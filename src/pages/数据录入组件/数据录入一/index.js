import { Outlet } from "react-router-dom"
import { Cascader, Rate,  Card,Space, AutoComplete, Checkbox, DatePicker, TimePicker, Select,Radio,Tag } from 'antd';
import styles from "./index.module.less"
import { DownOutlined ,UpOutlined} from '@ant-design/icons';
import { FrownOutlined, MehOutlined, SmileOutlined } from '@ant-design/icons';
import { useState } from "react";
const { RangePicker } = DatePicker;
const { Option } = Select;

/**
 * 
*/

const DataInputPageOne =()=>{
    return(
       <Space direction="vertical">
            <Space direction="horizontal">
                <AutoCompleteWidget/>
                <CascaderWidget/>
                <CheckBoxWidget/>
            </Space>
            <Space>
              <DatePickerWidget/>
              <RadioWidget/>
            </Space>
            <Space>
              <RateWidget/>
              <SelectWidget/>
            </Space>
       </Space>
    )
}

/**
 * 自动完成组件
 */
const AutoCompleteWidget=()=>{
    const [options,setOptions] = useState([])

    const handleSearch = (value)=>{
        setOptions(()=>{
            if(!value || value.includes("@")){
                return []
            }
            return ['gmail.com', '163.com', 'qq.com'].map((item,index)=>{
                return {
                    label:`${value}@${item}`,
                    value:`${value}@${item}`
                }
            })
        })
    }

    return(
        <Card title="AutoComplete 自动完成组件">
            <AutoComplete
                style={{width:200}}
                onSearch={handleSearch}
                placeholder="请输入邮箱"
                options={options}
            />
        </Card>
    )
}

/**
 * 级联选择
 */
const CascaderWidget =()=>{
    const options = [
        {
          value: 'zhejiang',
          label: 'Zhejiang',
          children: [
            {
              value: 'hangzhou',
              label: 'Hangzhou',
              children: [
                {
                  value: 'xihu',
                  label: 'West Lake',
                },
              ],
            },
          ],
        },
        {
          value: 'jiangsu',
          label: 'Jiangsu',
          children: [
            {
              value: 'nanjing',
              label: 'Nanjing',
              children: [
                {
                  value: 'zhonghuamen',
                  label: 'Zhong Hua Men',
                },
              ],
            },
          ],
        },
      ];

      const options2 = [
        {
          label: 'Light',
          value: 'light',
          children: new Array(20).fill(null).map((_, index) => ({
            label: `Number ${index}`,
            value: index,
          })),
        },
        {
          label: 'Bamboo',
          value: 'bamboo',
          children: [
            {
              label: 'Little',
              value: 'little',
              children: [
                {
                  label: 'Toy Fish',
                  value: 'fish',
                  disableCheckbox: true,
                },
                {
                  label: 'Toy Cards',
                  value: 'cards',
                },
                {
                  label: 'Toy Bird',
                  value: 'bird',
                },
              ],
            },
          ],
        },
      ];

      const onChange = (value) => {
        console.log(value);
      };

      return(
        <Card title="Cascader  级联选择组件" style={{width:"320px"}}>
            <Cascader 
                defaultValue={['zhejiang', 'hangzhou', 'xihu']} 
                options={options} 
                onChange={onChange} 
                style={{
                    width: '100%',
                    }}
            />
            <p/>
            <Cascader
                style={{
                width: '100%',
                }}
                options={options2}
                onChange={onChange}
                multiple
                maxTagCount="responsive"
            />
        </Card>
      )
}

/**
 * 复选框
 */
const CheckBoxWidget =()=>{

  const [value,setValue] = useState("")
  const [valueSec,setValueSec] = useState("")

  const plainOptions = ['语文', '英语', '数学'];

  const options = [
    { label: 'Apple', value: 'Apple' },
    { label: 'Pear', value: 'Pear' },
    { label: 'Orange', value: 'Orange' },
  ];

  const onChange =(checkValue)=>{
    setValue(checkValue)
  }

  const onChangeSec =(checkValue)=>{
    setValueSec(checkValue)
  }

  return(
    <Card title="Checkbox 多选框">
        <p>第一个选择了={value}</p>
        <Checkbox.Group options={options} defaultValue={["Apple"]} onChange={onChange}/>
        <p>第二个选择了={valueSec}</p>
        <Checkbox.Group options={plainOptions} defaultValue={["英语"]} onChange={onChangeSec}/>
    </Card>
  )
}


/**
 * 日历选择器
 */
const PickerWithType =({type,onChange})=>{
  if(type === "time") return <TimePicker onChange={onChange}/>
  if(type === "date") return <DatePicker onChange={onChange}/>
  return <DatePicker picker={type} onChange={onChange}/>
}
const DatePickerWidget =()=>{

  const onChange =(date,dateString)=>{
    console.log(dateString)
  }

  const [type,setType] = useState("time")

  return(
    <Card title="DatePicker 日期选择器">
      <Space direction="vertical">
        <DatePicker onChange={onChange}/>
        <RangePicker onChange={onChange}/>
        <Space>
          <Select value={type} onChange={setType}>
            <Option value="time">Time</Option>
            <Option value="date">Date</Option>
            <Option value="week">Week</Option>
            <Option value="month">Month</Option>
            <Option value="quarter">Quarter</Option>
            <Option value="year">Year</Option>
          </Select>
          <PickerWithType type={type} onChange={(value)=>console.log(value)}/>
        </Space>
      </Space>
    </Card>
  )
}

/**
 * 单选框 单选按键
 */
const RadioWidget =()=>{
  const plainOptions = ['Apple', 'Pear', 'Orange'];
  const options = [
    {
      label: 'Apple',
      value: 'Apple',
    },
    {
      label: 'Pear',
      value: 'Pear',
    },
    {
      label: 'Orange',
      value: 'Orange',
      title: 'Orange',
    },
  ];
  const optionsWithDisabled = [
    {
      label: 'Apple',
      value: 'Apple',
    },
    {
      label: 'Pear',
      value: 'Pear',
    },
    {
      label: 'Orange',
      value: 'Orange',
      disabled: true,
    },
  ];

  const [value1, setValue1] = useState('Apple');
  const [value2, setValue2] = useState('Apple');
  const [value3, setValue3] = useState('Apple');

  const onChange1 = ({ target: { value } }) => {
    console.log('radio1 checked', value);
    setValue1(value);
  };
  const onChange2 = ({ target: { value } }) => {
    console.log('radio2 checked', value);
    setValue2(value);
  };
  const onChange3 = ({ target: { value } }) => {
    console.log('radio3 checked', value);
    setValue3(value);
  };


  const [value,setValue] = useState("")
  const onChange =(e)=>{
    setValue(e.target.value)
  }
  return(
    <Card title="Radio  单选框">
      <Space>
        <Space direction="vertical">
          <p>选择了={value}</p>
          <Radio.Group onChange={onChange} value={value}>
            <Radio value={"A"}>A</Radio>
            <Radio value={"B"}>B</Radio>
            <Radio value={"C"}>C</Radio>
            <Radio value={"D"}>D</Radio>
          </Radio.Group>
        </Space>
        <Space direction="vertical">
          <Radio.Group options={plainOptions} onChange={onChange1} value={value1}/>
          <Radio.Group options={optionsWithDisabled} onChange={onChange2} value={value2} />
          <Radio.Group options={options} onChange={onChange3} value={value3} optionType="button" buttonStyle="solid"/>
          <Radio.Group defaultValue="a">
            <Radio.Button value="a">Hangzhou</Radio.Button>
            <Radio.Button value="b">Shanghai</Radio.Button>
            <Radio.Button value="c">Beijing</Radio.Button>
            <Radio.Button value="d">Chengdu</Radio.Button>
          </Radio.Group>
        </Space>
      </Space>
    </Card>
  )
}

/**
 * 评分组件
 */
const RateWidget =()=>{
  const customIcons = {
    1: <FrownOutlined />,
    2: <FrownOutlined />,
    3: <MehOutlined />,
    4: <SmileOutlined />,
    5: <SmileOutlined />,
  };

  return(
    <Card title="Rate 评分组件">
        <Space direction="vertical">
          <Rate allowHalf defaultValue={2.5} />
          <Rate defaultValue={2} character={({index = 0}) => customIcons[index+1]}/>
        </Space>
    </Card>
  )
}

/**
 * 选择器
 */
const SelectWidget =()=>{

  const options1 = [];
  for (let i = 10; i < 36; i++) {
    options1.push({
      label: i.toString(36) + i,
      value: i.toString(36) + i,
    });
  }

  const options =[
    {
      label:"jack",
      value:"jack"
    },
    {
      label:"jim",
      value:"jim"
    },
    {
      label:"tom",
      value:"tom"
    }
  ]

  const optionsTag = [
    {
      value: 'gold',
    },
    {
      value: 'lime',
    },
    {
      value: 'green',
    },
    {
      value: 'cyan',
    },
  ];

  const myTagRender=(props)=>{
    const {label,value,closable,onClose} = props
    const onPreventMouseDown =(event)=>{
      event.preventDefault();
      event.stopPropagation();
    }
    return(
      <Tag
        color={value}
        onMouseDown={onPreventMouseDown}
        closable={closable}
        onClose={onClose}
        style={{
          marginInlineEnd:4,
        }}
        >
        {label}
      </Tag>
    )
  }

  const onChange=(value)=>{
    console.log(`selected ${value}`)
  }
  return(
    <Card title="Select 选择器">
      <Space >
        <Space direction="vertical">
          <Select 
            defaultValue={"lucy"}
            style={{
              width:120
            }}
            onChange={onChange}
            options={options}
            />

            <Select
              defaultValue={"gaga"}
              disabled
              style={{
                width:120
              }}
              options={[
                {
                  value:"香蕉",
                  label:"gaga"
                }
              ]}
            />
        </Space>

        <Space direction="vertical">
          <Select
              defaultValue="lucy"
              style={{
                width: 120,
              }}
              loading
              options={[
                {
                  value: 'lucy',
                  label: 'Lucy',
                },
              ]}
            />

            <Select
              defaultValue="lucy"
              style={{
                width: 120,
              }}
              allowClear
              options={[
                {
                  value: 'lucy',
                  label: 'Lucy',
                },
              ]}
            />
        </Space>
        <Space direction="vertical">
            <Select 
              mode={"multiple"}
              allowClear
              style={{
                width:180
              }}
              placeholder={"请选择啊"}
              defaultValue={["a10","c12"]}
              onChange={onChange}
              options={options1}
            />  
            <Select
              mode="multiple"
              tagRender={myTagRender}
              defaultValue={["gold","cyan"]}
              style={{
                width:180
              }}
              options={optionsTag}
              />      
        </Space>
      </Space>
    </Card>
  )
}


export default DataInputPageOne