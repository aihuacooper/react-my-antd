import { Outlet } from "react-router-dom"

const NavPage =()=>{
    return(
        <Outlet/>
    )
}

export default NavPage