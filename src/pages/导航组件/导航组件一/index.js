import { Outlet } from "react-router-dom"
import { Button, Flex, Segmented, Steps, Card,Space, Dropdown, message, Pagination} from 'antd';
import styles from "./index.module.less"
import { DownOutlined ,UpOutlined} from '@ant-design/icons';

/**
 * 分页组件
 * 下拉菜单
*/

const NavPageOne =()=>{
    return(
       <Space direction="vertical">
        <DropDownWidget/>
        <PaginationWidget/>
        <StepsWidget/>
       </Space>
    )
}

/**
 * 步骤条
 */
const StepsWidget =()=>{
    return(
        <Card title="步骤条">
            <Space direction="horizontal">
                <Steps 
                    current={1}
                    items={[
                        {
                            title:"开始",
                            description:"我是介绍",
                        },
                        {
                            title:"进行中",
                            description:"我在干活中",
                        },
                        {
                            title:"结束",
                            description:"我已经干完了",
                        }
                    ]}
                />
                <Steps 
                    current={1}
                    direction="vertical"
                    items={[
                        {
                            title:"开始",
                            description:"我是介绍",
                        },
                        {
                            title:"进行中",
                            description:"我在干活中",
                        },
                        {
                            title:"结束",
                            description:"我已经干完了",
                        }
                    ]}
                />
            </Space>
        </Card>
    )
}


/**
 * 分页组件
 */
const PaginationWidget=()=>{
    return(
        <Card title="分页组件">
            <Space direction="vertical">
                <Pagination defaultCurrent={2} total={70}/>
                <Pagination 
                    defaultCurrent={3} 
                    total={80} 
                    showSizeChanger
                    showQuickJumper
                    />
                <Pagination 
                    defaultCurrent={3} 
                    total={80} 
                    disabled
                    showSizeChanger
                    />
                <Pagination simple defaultCurrent={2} total={70}/>
            </Space>
        </Card>
    )
}

/**
 * 下拉菜单
 */
const DropDownWidget=()=>{

    const items = [
        {
            key:"1",
            label:"选项一",
        },
        {
            key:"2",
            label:"选项二",
            disabled:true
        },
        {
            key:"3",
            label:"选项三",
            danger: true,
        },
        {
            key:"4",
            label:(<a href="https://www.baidu.com">百度</a>),
        }
    ]

    const onClick = ({key})=>{
        message.info(`点击了：${key}项`)
    }

    return(
        <Card className={styles.cardStyle} title="下拉菜单">
            <Space size={"large"}>
                <Dropdown menu={{items:items}} arrow >
                    <Button type="primary" icon={<DownOutlined />}>下拉菜单</Button>
                </Dropdown>
                <Dropdown menu={{items:items}} placement={"topCenter"} arrow>
                    <Button type="primary" icon={<UpOutlined />}>下拉菜单</Button>
                </Dropdown>
                <Dropdown menu={{items:items,onClick:onClick}} placement={"bottomRight"} arrow >
                    <Button type="primary" icon={<UpOutlined />}>下拉菜单-点击事件</Button>
                </Dropdown>
                <Dropdown menu={{items:items,onClick:onClick}} placement={"bottomRight"} arrow  trigger={['contextMenu']}>
                    <div
                        style={{
                        background: "#f0f8ff",
                        height: 200,
                        width:200,
                        textAlign: 'center',
                        lineHeight: '200px',
                        }}>
                        右击点出菜单
                    </div>
                </Dropdown>
            </Space>
        </Card>
        
    )


}

export default NavPageOne