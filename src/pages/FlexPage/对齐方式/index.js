import { Button, Flex, Segmented, Image, Card} from 'antd';
import { useState } from 'react';
import styles from "./index.module.less"

const justifyOptions = [
    'flex-start',
    'center',
    'flex-end',
    'space-between',
    'space-around',
    'space-evenly',
  ];

const alignOptions = ['flex-start', 'center', 'flex-end'];

const FlexAlignPage = () =>{

    const [justify,setJustify] = useState(justifyOptions[0])
    const [alignItems,setAlignItems] = useState(alignOptions[0])

    return(
        <Flex gap="small" align='start' vertical>
            <h3>选择justify横向对齐方式：</h3>
            <Segmented options={justifyOptions} onChange={setJustify} />
            <h3>选择align纵向对齐方式：</h3>
            <Segmented options={alignOptions} onChange={setAlignItems}/>
            <Flex className={styles.flexAlign} justify={justify} align={alignItems}>
                <Button type='primary'>按键</Button>
                <Button type='primary'>按键</Button>
                <Button type='primary'>按键</Button>
                <Button type='primary'>按键</Button>
                <Button type='primary'>按键</Button>
            </Flex>
            <Card className={styles.cardStyle} hoverable  styles={{body:{padding:0,overflow:"hidden"}}}>
            <Flex justify='space-between'>
                <Image className={styles.imgStyle}
                    width={230}
                    src='https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png'
                />
                <Flex className={styles.flexVer} vertical align='flex-end' justify='space-between'>
                    <p className={styles.pTextStyle} >"antd is an enterprise-class UI design language and React UI library"</p>
                    <Button className={styles.btStyle} type='primary'>Get Start</Button>
                </Flex>
            </Flex>
            </Card>
            
        </Flex>
    )
}
export default FlexAlignPage