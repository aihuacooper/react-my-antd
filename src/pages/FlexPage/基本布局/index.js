import {Flex, Radio} from "antd"
import { useState } from "react"
import styles from "./index.module.less"

const BaseFlexLayout = () =>{
    const [value,setValue] = useState("horizontal")
    return(
        <Flex gap="middle" vertical>
            <Radio.Group value={value} onChange={(e)=>setValue(e.target.value)}>
                <Radio value="horizontal">horizontal</Radio>
                <Radio value="vertical">vertical</Radio>
            </Radio.Group>
            <Flex vertical={value === "vertical"}>
                {Array.from({
                    length:4
                }).map((_,i)=>(
                    <div
                        key={i}
                        className={styles.divStyle}
                        style={{backgroundColor:i%2?'#1677ff' : '#1677ffbf',}}
                    >  第{i}条
                    </div>
                      
                ))}
            </Flex>
        </Flex>
    )
}

export default BaseFlexLayout