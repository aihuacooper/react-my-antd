import {Button, Flex} from "antd"
import { Outlet } from "react-router-dom"

const FlexPage =()=>{
    return(
        <Outlet/>
    )
}

export default FlexPage