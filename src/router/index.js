import HomeLayout from "@/pages/HomeLayout";
import ButtonPage from "@/pages/ButtonPage";
import FlexPage from "@/pages/FlexPage";
import BaseFlexLayout from "@/pages/FlexPage/基本布局";
import FlexAlignPage from "@/pages/FlexPage/对齐方式";
import GridPage from "@/pages/GridPage";
import GridBasePage from "@/pages/GridPage/基础布局";
import LayoutPage from "@/pages/LayoutPage";
import LayoutOne from "@/pages/LayoutPage/LayoutOne";
import LayoutTwo from "@/pages/LayoutPage/LayoutTwo";
import NavPage from "@/pages/导航组件";
import NavPageOne from "@/pages/导航组件/导航组件一";
import DataInputPageOne from "@/pages/数据录入组件/数据录入一";
import DataInputPageTwo from "@/pages/数据录入组件/数据录入二";
import DataInputPageThree from "@/pages/数据录入组件/数据录入三";
import DateShowWidget from "@/pages/数据展示组件/数据展示组件一"
import DateShowWidgetTwo from "@/pages/数据展示组件/数据展示组件二"
import FeedbackWidgetOne from "@/pages/反馈组件/反馈组件一"

const { createBrowserRouter } = require("react-router-dom");

const router= createBrowserRouter([
    {
        
        path:"/",
        element:<HomeLayout/>,
        children:[
            {
                index:true,
                element:<ButtonPage/>,
            },
            {
                path:"flex-page",
                element:<FlexPage/>,
                children:[
                    {
                        path:"flex-base-page",
                        element:<BaseFlexLayout/>
                    },
                    {
                        path:"flex-align-page",
                        element:<FlexAlignPage/>
                    }
                ]
            },
            {
                path:"grid-page",
                element:<GridPage/>,
                children:[
                    {
                        path:"grid-base-page",
                        element:<GridBasePage/>
                    },
                ]
            },
            {
                path:"layout-page",
                element:<LayoutPage/>,
                children:[
                    {
                        path:"layout-page-one",
                        element:<LayoutOne/>
                    },
                    {
                        path:"layout-page-two",
                        element:<LayoutTwo/>
                    },
                ]
            },
            {
                path:"nav-page",
                element:<NavPage/>,
                children:[
                    {
                        path:"nav-page-one",
                        element:<NavPageOne/>
                    },
                ]
            },
            {
                path:"data-input-page-one",
                element:<DataInputPageOne/>
            },
            {
                path:"data-input-page-two",
                element:<DataInputPageTwo/>
            },
            {
                path:"data-input-page-three",
                element:<DataInputPageThree/>
            },
            {
                path:"data-show-page-one",
                element:<DateShowWidget/>
            },
            {
                path:"data-show-page-two",
                element:<DateShowWidgetTwo/>
            },
            {
                path:"feedback-page-one",
                element:<FeedbackWidgetOne/>
            },
        ]
    }
])

export default router