const path = require('path')
const CracoLessPlugin = require('craco-less')

module.exports = {
  devServer: {
    port: 3009
  },
  webpack: {
    alias: {
      '@': path.resolve(__dirname, 'src')
    }
  },
  plugins:[
    {
      //less
      plugin: CracoLessPlugin
    },
  ]
}